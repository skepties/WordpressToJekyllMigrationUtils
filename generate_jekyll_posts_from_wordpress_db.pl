#!/usr/bin/env perl

use utf8;
use v5.10; # for say() function
use strict;
use warnings;
use Encode qw( encode decode );
 
use DBI;
use Data::Dumper;
 use File::Basename;
 
my %dirs = ( post=>'_posts', page=>'_pages', draft=>'_drafts' );
my %image_list;

# MySQL database configurations
my $dsn = "DBI:mysql:database=skepties_net;host=127.0.0.1;port=8889";
my $username = "root";
my $password = 'root';
 
# connect to MySQL database
my %attr = ( PrintError=>0,  # turn off error reporting via warn()
             RaiseError=>1   # report error via die()
           );
my $dbh = DBI->connect($dsn,$username,$password,\%attr);

my $sql;;
my $sth;

### pre
my $sql_postids = qq/select id from wp_posts where post_type IN ('post','page')/;

### Meta
my %meta;
$sql = qq/select * from wp_postmeta where post_id IN ($sql_postids);/;
$sth = $dbh->prepare($sql);
$sth->execute() or die "execution failed: $dbh->errstr()";
while (my $ref = $sth->fetchrow_hashref()) {
  push @{$meta{$ref->{post_id}}{$ref->{meta_key}}}, $ref->{meta_value};
}

### Featured image;
my %featured_image;
$sql = qq/select post_id, post_content, post_excerpt, guid from wp_postmeta m, wp_posts p where meta_key="_thumbnail_id" and p.id=meta_value and post_id IN ($sql_postids)/;
$sth = $dbh->prepare($sql);
$sth->execute() or die "execution failed: $dbh->errstr()";
while (my $ref = $sth->fetchrow_hashref()) {
  push @{$featured_image{$ref->{post_id}}}, $ref;
}


### terms
my %term;
$sql = qq/select t.term_id, name, taxonomy from wp_terms t, wp_term_taxonomy t2 where t.term_id=t2.term_id and taxonomy IN ('category','post_tag');/;
$sth = $dbh->prepare($sql);
$sth->execute() or die "execution failed: $dbh->errstr()";
while (my $ref = $sth->fetchrow_hashref()) {
    $term{$ref->{term_id}} = $ref;
}

### post - terms
my %post_term;
$sql = qq/select * from wp_term_relationships where object_id IN ($sql_postids);/;
$sth = $dbh->prepare($sql);
$sth->execute() or die "execution failed: $dbh->errstr()";
while (my $ref = $sth->fetchrow_hashref()) {
  push @{$post_term{$ref->{object_id}}}, $ref->{term_taxonomy_id};
}

$sql = qq/select  p.id, post_author, post_type, post_date, post_title, post_status, post_content_filtered, user_nicename
from wp_posts p, wp_users u
where post_type IN ('post','page') and post_author=u.ID;/;
$sth = $dbh->prepare($sql);
$sth->execute() or die "execution failed: $dbh->errstr()";

mkdir for values %dirs;
while (my $ref = $sth->fetchrow_hashref()) {
  my $id     = $ref->{"id"};
  my $title  = $ref->{post_title};
  my $date   = $ref->{post_date};
  my $type   = $ref->{post_type};
  my $author = $ref->{user_nicename};
  my $status = $ref->{post_status};
  my $data   = $ref->{post_content_filtered};
  my @category;
  my @tag;
  for my $tid ( @{ $post_term{$id} } ){
    my $t = $term{$tid};
    push @category, $t->{name} if $t->{taxonomy} eq 'category';
    push @tag,      $t->{name} if $t->{taxonomy} eq 'post_tag';
  }
  my $o_author = $meta{$id}{sk_orignal_author}[0];
  my $o_source = $meta{$id}{sk_orignal_source}[0];
  my $img = $featured_image{$id}[0]{guid};
  my $img_desc = $featured_image{$id}[0]{post_excerpt};
  my $permalink;
  if( $type eq 'post' ){ $permalink = "/p/$id"; }

  $img = "http://skepties.net$img" if $img && $img =~ /^\// ;
  $image_list{$img}++ if $img;
  say $img if $img;
  my $safe_word = '___SAFE___WORD___'.time.rand.rand.rand.rand.rand;
  my @safe_queue;
  #while( $data =~ s/(<\s*img[^<>]+src="?|!\[[^\]]*]\()([^"')]+)/$1.'{{ site.baseurl }}\/assets\/img\/'.basename($2)/e ){
  while( $data =~ s/(<\s*img[^<>]+src="?|!\[[^\]]*]\()([^"')]+)/$safe_word/ ){
    push @safe_queue, $1.'{{ site.baseurl }}/assets/img/'.basename($2);
    my $im = $2;
    $im = "http://skepties.net$2" if $2 =~/^\//;;
    $image_list{$im}++;
    #say "$1$2\t$safe_queue[-1]";
  }
  $data =~s/$safe_word/shift @safe_queue/ge;

  my $str = << "EOFF";
---
layout: $type
title: "$title"
date: $date
type: $type
status: $status
author: $author
EOFF
  $str .= 'published: '.($status eq 'publish'?'true':'false')."\n";
  if( @category ){
    $str .= "categories: \n";
    $str .= "  - $_\n" for @category;
  }
  if( @tag ){
    $str .= "tags: \n";
    $str .= "  - $_\n" for @tag;
  }
  $str .= "img: $img\n" if $img;
  $str .= qq/img_desc: '$img_desc'\n/ if $img_desc;
  $str .= "o_author: $o_author\n" if $o_author;
  $str .= "o_source: $o_source\n" if $o_source;

  $str .= "---\n$data";

  say "===== $ref->{post_title}";
  my ($date_head) = $date=~/^(\S+)/g;
  my $filename = decode("utf-8",$title);
  #binmode STDOUT, "encoding(utf8)";
  $filename =~ s/[)]//g;
  $filename =~ s/\P{Letter}+/-/g;
  $filename =~ s/^-+//;
  $filename =~ s/(^.{30}).*/$1/;
  $filename =~ s/-+$//;
  #$filename = decode('utf-16',$filename);
  #$filename = decode("utf-8",$filename);


  my $ty2;
  $ty2 = 'post' if $type eq 'post';
  $ty2 = 'page' if $type eq 'page';
  $ty2 = 'draft' if $status eq 'draft';
  if( $ty2 ){
    $filename = "$dirs{$ty2}/$date_head-$filename.md";
    open my $fh, '>', $filename;
    print $fh $str;
    close $fh;

  }else{
    say "\n!!!!!!!!!! $filename\n";
  }
  open my $fh, '>', "image_list.txt";
  print $fh $_,"\n" for keys %image_list;
  close  $fh;
}



# disconnect from the MySQL database
$dbh->disconnect();
